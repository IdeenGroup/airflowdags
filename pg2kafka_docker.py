from datetime import timedelta, datetime
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.providers.docker.operators.docker import DockerOperator
from uuid import uuid4

import logging

log = logging.getLogger(__name__)


with DAG(
    dag_id='docker_compose_up',
    schedule_interval='0 0 * * *',
    start_date=datetime(2021, 1, 1),
    catchup=False,
    dagrun_timeout=timedelta(minutes=60),
) as dag:
    bash_command_1 = BashOperator(
        task_id='docker-ps',
        bash_command='sshpass -p "ideen" ssh -oStrictHostKeyChecking=no -p 2021 ideen@176.53.197.134 "cd /home/poroshin/ideen/projects/postgrestokafkatransfer; docker-compose up -d"',
    )


if __name__ == "__main__":
    dag.cli()
